# contact book app

- add contact
- remove contact
- add info about contact (eg. email: foo@bar.baz)
- update info (with the ability to undo changes)
- remove info
- step back

Generated using Vue-cli
```
$ npm install

$ npm run dev
```

### Demo

https://adam-it.ru