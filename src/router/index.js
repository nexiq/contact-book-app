import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'contact-list',
    component: () => import('../views/ContactList')
  },
  {
    path: '/contact/:id',
    name: 'contact',
    component: () => import(/* webpackChunkName: "contact" */ '../views/Contact.vue')
  },
  {
    path: '/*',
    component: () => import('../views/NotFound')
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
