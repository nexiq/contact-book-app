import Vue from 'vue'
import Vuex from 'vuex'

import MainStore from './main/main-store'

Vue.use(Vuex)

export default new Vuex.Store({
  state: MainStore.state,
  mutations: MainStore.mutations,
  actions: MainStore.actions,
  getters: MainStore.getters,
  modules: {}
})
