import Vue from 'vue';

export default {
  state: {
    contacts: JSON.parse(localStorage.getItem('contacts') || '[]'),
    history: []
  },
  getters: {
    contacts: s => s.contacts,

    contactById: s => id => s.contacts.find(contact => contact.id === id),
  },
  mutations: {
    addContact(state, contact) {
      state.contacts.push(contact)
    },

    removeContact(state, contactId) {
      state.contacts = state.contacts.filter(({ id }) => id !== contactId)
    },

    addContactInfo(state, { data, contactId }) {
      const contact = state.contacts.find(({ id }) => id === contactId)

      contact.info.push(data)
    },

    removeInfoField(state, { contactId, fieldId }) {
      const contact = state.contacts.find(({ id }) => id === contactId)
      contact.info = contact.info.filter(({ id }) => id !== fieldId);
    },

    updateInfoField(state, { contactId, fieldId, newValue }) {
      const contact = state.contacts.find(({ id }) => id === contactId)
      const fieldIdx = contact.info.findIndex(({ id }) => id === fieldId)

      // TODO: If troubles with reactive
      Vue.set(contact.info, fieldIdx, { id: fieldId, ...newValue })
    },

    saveStep(state) {
      state.history.push(JSON.parse(JSON.stringify(state.contacts)))
    },

    stepBack(state) {
      state.contacts = state.history.pop()
    },

    clearHistory(state) {
      state.history = []
    }
  },
  actions: {
    addContact({ commit }, contact) {
      commit('addContact', contact)
    },

    removeContact({ commit }, id) {
      commit('removeContact', id)
    },

    addContactInfo({ commit }, data) {
      commit('addContactInfo', data)
    },

    removeInfoField({ commit }, data) {
      commit('removeInfoField', data)
    },

    updateInfoField({ commit }, data) {
      commit('updateInfoField', data)
    },

    saveStep({commit}) {
      commit('saveStep')
    },

    stepBack({commit}) {
      commit('stepBack')
    },

    clearHistory({commit}) {
      commit('clearHistory')
    }
  },
}

